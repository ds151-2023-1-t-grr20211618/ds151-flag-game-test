import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
  const [username, setUsername] = useState('');

  const iniciarPress = () => {
    navigation.navigate("Game", {
      username: username
    });
  };

  const outroBotaoPress = () => {
    navigation.navigate("Time", {
      username: username
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Bem-vindo</Text>
      <View style={styles.container_name}>
        <Text style={styles.labelName}>Digite seu nome</Text>
        <TextInput 
          style={styles.textInput}
          value={username}
          onChangeText={(t) => setUsername(t)}
        />
        <View style={styles.buttonContainer}>
          <Button 
            title="Modo Original"
            color="#0a0"
            disabled={username === ''}
            onPress={iniciarPress}
          />
          <Button 
            title="Modo Temporizado"
            color="#00f"
            onPress={outroBotaoPress}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcome: {
    fontSize: 50,
    color: '#004',
    fontFamily: 'monospace',
    textTransform: 'uppercase',
  },
  container_name: {
    justifyContent: 'center',
  },
  labelName: {
    fontSize: 30,
    fontFamily: 'monospace',
  },
  textInput: {
    borderWidth: 2,
    margin: 20,
    borderColor: '#008',
    borderRadius: 20,
    padding: 20,
    fontSize: 20,
    fontFamily: 'monospace'
  },
  buttonContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
  },
});

export default HomeScreen;